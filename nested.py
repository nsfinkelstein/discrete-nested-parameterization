import numpy as np
import sympy as sy

from functools import reduce
from operator import mul, add
from ananke.graphs import ADMG, IG
from collections import OrderedDict as dict
from itertools import chain, product, combinations


def statespace(cardinalities):
    return (
        dict(zip(cardinalities.keys(), p))
        for p in product(*[range(v) for v in cardinalities.values()])
    )


def get_probability_expressions(admg, cardinalities):
    cardinalities = dict(sorted(cardinalities, key=lambda x: x[0]))
    heads_tails = get_heads_tails(admg)
    return {
        tuple(outcome.values()): outcome_probability(outcome, cardinalities, heads_tails)
        for outcome in statespace(cardinalities)
        if outcome != {v: 0 for v in cardinalities}
    }


def get_distribution(parameter_values, cardinalities, expressions=None, admg=None, constraints=None, check_constraints=False):
    # maps nested parameterization to a probabilit distribution
    # NOTE: cardinalities can be inferred from parameters...
    cardinalities = dict(sorted(cardinalities, key=lambda x: x[0]))
    parameter_values = dict((sy.Symbol(k), v) for k, v in dict(parameter_values).items())

    expressions = expressions or get_probability_expressions(admg, cardinalities)
    dist = np.zeros(tuple(cardinalities.values()))
    for key, value in expressions.items():
        dist[key] = value.subs(parameter_values)

    dist[tuple(0 for _ in cardinalities)] = 1 - np.sum(dist)
    return dist


def parameterize(admg, cardinalities, constraints=False):
    """
    example:
    >> admg = ((('A', 'B'), ('B', 'C'), ('C', 'D')), (('B', 'D'),))
    >> parameterize(admg)
    """
    parameters = set()
    heads_tails = get_heads_tails(admg)
    if type(cardinalities) is not dict:
        cardinalities = dict(sorted(cardinalities, key=lambda x: x[0]))

    for outcome in statespace(cardinalities):
        if outcome == {v: 0 for v in cardinalities}: continue
        outcome_expression = outcome_probability(outcome, cardinalities, heads_tails)
        parameters.update(outcome_expression.free_symbols)

    return list(sorted(parameters, key=str))


def outcome_probability(outcome, cardinalities, heads_tails):
    # see Appendix C, Evans and Richardson 2017
    if type(cardinalities) is not dict:
        cardinalities = dict(sorted(cardinalities, key=lambda x: x[0]))
    o = {var for var, val in outcome.items() if val != 0}
    return sum((-1)**(len(c - o)) * sum(prod(sy.Symbol('q{}({}{}{})'.format(
                    reduce(add, h, ''),
                    ','.join('{}={}'.format(v, y[v]) for v in h),
                    '' if len(t) == 0 else '|',
                    ','.join('{}={}'.format(v, outcome[v]) for v in t),
                )) for h, t in partition(c, heads_tails)
            ) for y in get_ys(c, o, outcome, cardinalities)
        ) for c in get_cs(o, outcome.keys()))


def get_cs(o, nodes):
    return (x | o for x in powerset(set(nodes) - o))


def get_ys(c, o, outcome, cardinalities):
    return (
        outprime for outprime in
        statespace(dict((n, cardinalities[n]) for n in c))
        if all(outcome[n] == outprime[n] for n in o)
            and 0 not in outprime.values()
    )


def powerset(x):
    return (set(c) for i in range(len(x) + 1) for c in combinations(x, i))


def rchain(xs):
    if not all(type(x) in (list, tuple, set) for x in xs): return xs
    return chain.from_iterable(rchain(x) for x in xs)


def prod(xs):
    return reduce(mul, xs, 1)


def get_heads_tails(admg):
    g = IG(ADMG(set(rchain(admg)), admg[0], admg[1]))
    g.get_intrinsic_sets()  # necessary side-effects for heads-tails, i think
    return [(tuple(sorted(h)), tuple(sorted(t))) for h, t in g.get_heads_tails()]


def partition(nodes, heads_tails):
    if len(nodes) == 0: return set()
    phi = get_phi(nodes, heads_tails)
    psi = get_psi(nodes, phi)
    return phi | partition(psi, heads_tails)


def get_phi(nodes, heads_tails):
    # see Definition 4.11, Evans and Richardson 2017)
    heads = set((h, t) for h, t in heads_tails if set(h).issubset(set(nodes)))
    return set(
        (h, t) for h, t in heads
        if not any(h != h2 and set(h).issubset(set(h2)) for h2, t2 in heads)
    )


def get_psi(nodes, phi):
    # see Definition 4.11, Evans and Richardson 2017
    return sorted(tuple(set(nodes) - set(chain.from_iterable(h for h, t in phi))))
